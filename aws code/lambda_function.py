# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import time
import boto3
import numpy as np
import tensorflow as tf
import json
import PIL.Image as Image

s3 = boto3.resource('s3')

def load_graph(model_file):
  graph = tf.Graph()
  graph_def = tf.GraphDef()
  print(model_file)
  with open(model_file, "rb") as f:
    graph_def.ParseFromString(f.read())
  with graph.as_default():
    tf.import_graph_def(graph_def)

  return graph

def read_tensor_from_image_file(file, input_height=299, input_width=299,
				input_mean=0, input_std=255):
  input_name = "file_reader"
  output_name = "normalized"
  print("line 46")
  image_array = np.array(file)[:, :, 0:3]  # Select RGB channels only.
 # image_reader = tf.image.decode_jpeg({'DecodeJpeg:0': image_array}, channels = 3,
                                       # name='jpeg_reader')
  float_caster = tf.cast(image_array, tf.float32)
  dims_expander = tf.expand_dims(float_caster, 0);
  resized = tf.image.resize_bilinear(dims_expander, [input_height, input_width])
  normalized = tf.divide(tf.subtract(resized, [input_mean]), [input_std])
  sess = tf.Session()
  result = sess.run(normalized)

  return result

def readImageFromBucket(key, bucket_name):
  print(s3)
  bucket = s3.Bucket(bucket_name)
  object = bucket.Object(key)
  print("yahello")
  response = object.get()
  print("got past response")
  return Image.open(response["Body"])

def load_labels(label_file):
  label = []
  proto_as_ascii_lines = tf.gfile.GFile(label_file).readlines()
  for l in proto_as_ascii_lines:
    label.append(l.rstrip())
  return label



def lambda_handler(event, context):
  print("hi")
  model_file = "retrained_graph.pb"
  print("here")
  label_file = "retrained_labels.txt"
  input_height = 224
  input_width = 224
  input_mean = 128
  input_std = 128
  input_layer = "input"
  output_layer = "final_result"
  print("here")
  bucket_name = event['Records'][0]['s3']['bucket']['name']
  key = event['Records'][0]['s3']['object']['key']
  print(bucket_name)
  img = readImageFromBucket(key, bucket_name)
  print("wtf")
  graph = load_graph(model_file)
  t = read_tensor_from_image_file(img,
                                  input_height=input_height,
                                  input_width=input_width,
                                  input_mean=input_mean,
                                  input_std=input_std)
  print("yooooooo")
  input_name = "import/" + input_layer
  output_name = "import/" + output_layer
  input_operation = graph.get_operation_by_name(input_name);
  output_operation = graph.get_operation_by_name(output_name);

  with tf.Session(graph=graph) as sess:
    start = time.time()
    results = sess.run(output_operation.outputs[0],
                      {input_operation.outputs[0]: t})
    end=time.time()
  results = np.squeeze(results)

  top_k = results.argsort()[-27:][::-1]
  labels = load_labels(label_file)

  print('\nEvaluation time (1-image): {:.3f}s\n'.format(end-start))
  template = "{} (score={:0.5f})"
  for i in top_k:
    print("  "+template.format(labels[i], results[i]))
  
    if labels[i] == "compost":
      if results[i] > 0.85:
       #return true
      else: return false