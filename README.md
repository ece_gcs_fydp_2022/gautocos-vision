# Gautocos Vision

Python code responsible for computer vision

## Dependencies
- Python 3.7 64bit
- Tensorflow 1.15

You can install tensorflow with

`py -3.7-64 -m pip install tensorflow==1.15`

## To run the model prediction on an image

e.g.

`python -m scripts.label_image --graph=tf_files/retrained_graph.pb --image=data/test/compost9.jpg`

or if you have multiple python versions installed

`py -3.7-64 -m scripts.label_image --graph=tf_files/retrained_graph.pb --image=data/test/compost9.jpg`
