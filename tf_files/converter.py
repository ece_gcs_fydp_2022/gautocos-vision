import tensorflow as tf

converter = tf.compat.v1.lite.TFLiteConverter.from_frozen_graph(
    graph_def_file='retrained_graph.pb',
                    # both `.pb` and `.pbtxt` files are accepted.
    input_arrays=['input'],
    output_arrays=['final_result']
)
tflite_model = converter.convert()

# Save the model.
with open('model.tflite', 'wb') as f:
  f.write(tflite_model)